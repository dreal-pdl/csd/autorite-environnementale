#
# This is the user-interface definition of a Shiny web application. You can
# run the application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#

sidebar <- dashboardSidebar(
  sidebarMenu(
    menuItem("Accueil", tabName = "home", icon = icon('home')),
    selectInput("select_year","Année :",c(year(today()):2016),selected=year(today())),
    menuItem('Bilans d\'activité', tabName = 'menuOne', icon = icon('table'), 
             menuSubItem('Avis AE - Plans/Programmes', tabName = 'bilan_activite_aepp'),
             menuSubItem(HTML('Décisions cas par cas -<br/>Plans/Programmes'), tabName = 'bilan_activite_aepp_cc'),
             menuSubItem('Avis AE - Projets', tabName = 'bilan_aepr'),
             menuSubItem('Décisions cas par cas - Projets', tabName = 'bilan_aepr_cc')
    ),
    menuItem('Statistiques', tabName = 'stats', icon = icon('chart-bar'), 
             menuSubItem('Avis AE - Plans/Programmes', tabName = 'stats_aepp'),
             menuSubItem(HTML('Décisions cas par cas -<br/>Plans/Programmes'), tabName = 'stats_aepp_cc'),
             menuSubItem('Avis AE - Projets', tabName = 'stats_aepr'),
             menuSubItem('Décisions cas par cas - Projets', tabName = 'stats_aepr_cc')
    ),
    menuItem("Évolutions", tabName = "evolutions", icon = icon('random'),
             menuSubItem('Années précédentes', tabName = 'previous_years'),
             menuSubItem('Année sélectionnée', tabName = 'selected_year')
             ),
    menuItem("Aide", tabName = "help", icon = icon('question-circle'),
             menuSubItem('Présentation', tabName = 'presentation'),
             menuSubItem('Glossaire', tabName = 'glossary')
             
    ),
    menuItem("Développement", tabName = "dev", icon = icon('edit'),
             menuSubItem('Journal des modifications', tabName = 'changelog'),
             menuSubItem('Feuille de route', tabName = 'roadmap')    
   ),
   menuItem("Mentions légales",tabName = "legal_notice",icon=icon("gavel")),
   menuItem("Nous contacter",icon=icon("envelope"),href="mailto:adl.dreal-pdl@developpement-durable.gouv.fr?subject=Application Autorité environnementale&body=Bonjour,"),   
   tags$br(),
   tags$img(src="logo_datalab_blanc.svg",width="150px",style="display: block; margin-left: auto; margin-right: auto;"),
   tags$br(),
   tags$img(src="logo_pref_dreal_2019.jpg", width="120px",style="display: block; margin-left: auto; margin-right: auto;")
   )
)

body <- dashboardBody(
  #Page d'accueil : chiffres-clés
  tabItems(
    tabItem(tabName = "home",
            h1("Bilans d'activité et tableaux de bord de l'Autorité environnementale"),
            h2(textOutput("title_year")),
            p(textOutput("date_export")),
            h3("Avis de l'AE locale sur les Plans/Programmes"),
            fluidRow(column(width = 12, infoBoxOutput("folderAeppBox"), infoBoxOutput("avisSigneAeppBox"), infoBoxOutput("avisTaciteAeppBox") %>% 
                              withSpinner(type = getOption("spinner.type", default = 8),size = getOption("spinner.size", default = 1)))),
            h3("Décisions de l'AE locale sur les cas par cas Plans/Programmes"),
            fluidRow(column(width = 12, infoBoxOutput("folderAeppCcBox"), infoBoxOutput("newfolderAeppCcBox"), infoBoxOutput("soumissionAeppCcBox") %>% 
                              withSpinner(type = getOption("spinner.type", default = 8),size = getOption("spinner.size", default = 1)))),
            h3("Avis de l'AE locale sur les projets"),
            fluidRow(column(width = 12, infoBoxOutput("folderAeprBox"), infoBoxOutput("avisSigneAeprBox"), infoBoxOutput("avisTaciteAeprBox") %>% 
                              withSpinner(type = getOption("spinner.type", default = 8),size = getOption("spinner.size", default = 1)))),
            h3("Décisions de l'AE locale sur les cas par cas projets"),
            fluidRow(column(width = 12, infoBoxOutput("folderAeprCcBox"), infoBoxOutput("newfolderAeprCcBox"), infoBoxOutput("soumissionAeprCcBox") %>% 
                              withSpinner(type = getOption("spinner.type", default = 8),size = getOption("spinner.size", default = 1))))
            ),
    #Bilans d'activité
    tabItem(tabName = "bilan_activite_aepp",
            h2(textOutput("title_bilan_activite_aepp")),
            tabsetPanel(
              id = 'dataset',
              tabPanel("Trimestre 1", fluidRow(column(width = 12,br(),DT::dataTableOutput("aepp_trim_1")))),
              tabPanel("Trimestre 2", fluidRow(column(width = 12,br(),DT::dataTableOutput("aepp_trim_2")))),
              tabPanel("Trimestre 3", fluidRow(column(width = 12,br(),DT::dataTableOutput("aepp_trim_3")))),
              tabPanel("Trimestre 4", fluidRow(column(width = 12,br(),DT::dataTableOutput("aepp_trim_4")))),
              tabPanel("Annuel", fluidRow(column(width = 12,br(),DT::dataTableOutput("aepp_annuel")))
              )
            )
    ),
    tabItem(tabName = "bilan_activite_aepp_cc",
            h2(textOutput("title_bilan_activite_aepp_cc")),
            tabsetPanel(
              id = 'dataset_2',
              tabPanel("Trimestre 1", fluidRow(column(width = 12, br(), DT::dataTableOutput("aepp_cc_trim_1")))),
              tabPanel("Trimestre 2", fluidRow(column(width = 12, br(), DT::dataTableOutput("aepp_cc_trim_2")))),
              tabPanel("Trimestre 3", fluidRow(column(width = 12, br(), DT::dataTableOutput("aepp_cc_trim_3")))),
              tabPanel("Trimestre 4", fluidRow(column(width = 12, br(), DT::dataTableOutput("aepp_cc_trim_4")))),
              tabPanel("Annuel", fluidRow(column(width = 12, br(), DT::dataTableOutput("aepp_cc_annuel")))
              )
            )
    ),    
    tabItem(tabName = "bilan_aepr",
            h2(textOutput("title_bilan_activite_aepr")),
            tabsetPanel(
              id = 'dataset_3',
              tabPanel("Trimestre 1", fluidRow(column(width = 12, br(), DT::dataTableOutput("aepr_trim_1")))),
              tabPanel("Trimestre 2", fluidRow(column(width = 12, br(), DT::dataTableOutput("aepr_trim_2")))),
              tabPanel("Trimestre 3", fluidRow(column(width = 12, br(), DT::dataTableOutput("aepr_trim_3")))),
              tabPanel("Trimestre 4", fluidRow(column(width = 12, br(), DT::dataTableOutput("aepr_trim_4")))),
              tabPanel("Annuel", fluidRow(column(width = 12, br(), DT::dataTableOutput("aepr_annuel"))))
            )            
    ), 
    tabItem(tabName = "bilan_aepr_cc",
            h2(textOutput("title_bilan_activite_aepr_cc")),
            tabsetPanel(
              id = 'dataset_4',
              tabPanel("Trimestre 1", fluidRow(column(width = 12, br(), DT::dataTableOutput("aepr_cc_trim_1")))),
              tabPanel("Trimestre 2", fluidRow(column(width = 12, br(), DT::dataTableOutput("aepr_cc_trim_2")))),
              tabPanel("Trimestre 3", fluidRow(column(width = 12, br(), DT::dataTableOutput("aepr_cc_trim_3")))),
              tabPanel("Trimestre 4", fluidRow(column(width = 12, br(), DT::dataTableOutput("aepr_cc_trim_4")))),
              tabPanel("Annuel", fluidRow(column(width = 12, br(), DT::dataTableOutput("aepr_cc_annuel")))
              )
            )            
    ),
    #Statistiques
    tabItem(tabName = "stats_aepp",
            h2(textOutput("title_stats_aepp")),
            tabsetPanel(
              tabPanel("Trimestre 1", 
                       fluidRow(
                         column(width = 12, br(),
                                box(h3("Répartition par type d'avis et de dossiers"), status = "info", tableOutput('aepp_type_avis_trim_1')),
                                box(h3("Répartition par département "), status = "info", 
                                    h4("Documents d'urbanisme"), tableOutput('aepp_departement_du_trim_1'),
                                    h4("Autres Plans/Programmes"), tableOutput('aepp_departement_autres_trim_1'))                           
                         )
                       )
              ),
              tabPanel("Trimestre 2", 
                       fluidRow(
                         column(width = 12, br(),
                                box(h3("Répartition par type d'avis et de dossiers"), status = "info", tableOutput('aepp_type_avis_trim_2')),
                                box(h3("Répartition par département"), status = "info", 
                                    h4("Documents d'urbanisme"), tableOutput('aepp_departement_du_trim_2'),
                                    h4("Autres Plans/Programmes"), tableOutput('aepp_departement_autres_trim_2'))
                         )
                       )
              ),
              tabPanel("Trimestre 3", 
                       fluidRow(
                         column(width = 12, br(),
                                box(h3("Répartition par type d'avis et de dossiers"), status = "info", tableOutput('aepp_type_avis_trim_3')),
                                box(h3("Répartition par département"), status = "info", 
                                    h4("Documents d'urbanisme"), tableOutput('aepp_departement_du_trim_3'),
                                    h4("Autres Plans/Programmes"), tableOutput('aepp_departement_autres_trim_3'))
                         )
                       )
              ),
              tabPanel("Trimestre 4", 
                       fluidRow(
                         column(width = 12, br(),
                                box(h3("Répartition par type d'avis et de dossiers"), status = "info", tableOutput('aepp_type_avis_trim_4')),
                                box(h3("Répartition par département"), status = "info", 
                                    h4("Documents d'urbanisme"), tableOutput('aepp_departement_du_trim_4'),
                                    h4("Autres Plans/Programmes"), tableOutput('aepp_departement_autres_trim_4'))
                         )
                       )
              ),
              tabPanel("Annuel", 
                       fluidRow(
                         column(width = 12, br(),
                                box(h3("Répartition par type d'avis et de dossiers"), status = "info", tableOutput('aepp_type_avis_annuel')),
                                box(h3("Répartition par département"), status = "info", 
                                    h4("Documents d'urbanisme"), tableOutput('aepp_departement_annuel_du'),
                                    h4("Autres Plans/Programmes"), tableOutput('aepp_departement_annuel_autres'))
                         )
                       )
              ),
              tabPanel("Graphiques", 
                       fluidRow(
                         column(width = 12, h3("Répartition annuelle du nombre de dossiers")),
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepp_departement")),
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepp_domaine"))
                       ),
                       fluidRow(
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepp_categorie")),
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepp_procedure"))
                       )
              )
            )
    ),
    tabItem(tabName = "stats_aepp_cc",
            h2(textOutput("title_stats_aepp_cc")),
            tabsetPanel(
              tabPanel("Trimestre 1", 
                       fluidRow(
                         column(
                           width = 12,br(),
                           box(h3("Décisions signées"), status = "info", tableOutput('aepp_cc_decisions_signees_trim_1'),
                               h3("Recours"), tableOutput('aepp_cc_recours_trim_1')
                           ),
                           box(
                             h3("Sens des décisions sur le total des dossiers AEPP-CC"), status = "info",
                             h4("Cas par cas Documents d'urbanisme"), tableOutput('aepp_cc_sens_decisions_du_trim_1'),
                             h4("Cas par cas autres Plans/Programmes"), tableOutput('aepp_cc_sens_decisions_autres_trim_1')
                           )
                         )
                       )
              ),
              tabPanel("Trimestre 2", 
                       fluidRow(
                         column(
                           width = 12,br(),
                           box(h3("Décisions signées"), status = "info", tableOutput('aepp_cc_decisions_signees_trim_2'),
                               h3("Recours"), tableOutput('aepp_cc_recours_trim_2')
                           ),
                           box(
                             h3("Sens des décisions sur le total des dossiers AEPP-CC"), status = "info",
                             h4("Cas par cas Documents d'urbanisme"), tableOutput('aepp_cc_sens_decisions_du_trim_2'),
                             h4("Cas par cas autres Plans/Programmes"), tableOutput('aepp_cc_sens_decisions_autres_trim_2')
                           )
                         )
                       )
              ),
              tabPanel("Trimestre 3", 
                       fluidRow(
                         column(
                           width = 12,br(),
                           box(h3("Décisions signées"), status = "info", tableOutput('aepp_cc_decisions_signees_trim_3'),
                               h3("Recours"), tableOutput('aepp_cc_recours_trim_3')
                           ),
                           box(
                             h3("Sens des décisions sur le total des dossiers AEPP-CC"), status = "info",
                             h4("Cas par cas Documents d'urbanisme"), tableOutput('aepp_cc_sens_decisions_du_trim_3'),
                             h4("Cas par cas autres Plans/Programmes"), tableOutput('aepp_cc_sens_decisions_autres_trim_3')
                           )
                         )
                       )
              ),
              tabPanel("Trimestre 4", 
                       fluidRow(
                         column(
                           width = 12,br(),
                           box(h3("Décisions signées"), status = "info", tableOutput('aepp_cc_decisions_signees_trim_4'),
                               h3("Recours"), tableOutput('aepp_cc_recours_trim_4')
                           ),
                           box(
                             h3("Sens des décisions sur le total des dossiers AEPP-CC"), status = "info",
                             h4("Cas par cas Documents d'urbanisme"), tableOutput('aepp_cc_sens_decisions_du_trim_4'),
                             h4("Cas par cas autres Plans/Programmes"), tableOutput('aepp_cc_sens_decisions_autres_trim_4')
                           )
                         )
                       )
              ),
              tabPanel("Annuel", 
                       fluidRow(
                         column(
                           width = 12,br(),
                           box(h3("Décisions signées"), status = "info", tableOutput('aepp_cc_decisions_signees_total_annuel'),
                               h3("Recours"), tableOutput('aepp_cc_recours_total_annuel')
                           ),
                           box(
                             h3("Sens des décisions sur le total des dossiers AEPP-CC"), status = "info",
                             h4("Cas par cas Documents d'urbanisme"), tableOutput('aepp_cc_sens_decisions_du_total_annuel'),
                             h4("Cas par cas autres Plans/Programmes"), tableOutput('aepp_cc_sens_decisions_autres_total_annuel')
                           )
                         )
                       )
              ),
              tabPanel("Graphiques", 
                       fluidRow(
                         column(width = 12, h3("Répartition annuelle du nombre de dossiers")),
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepp_cc_departement")),
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepp_cc_domaine"))
                       ),
                       fluidRow(
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepp_cc_categorie")),
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepp_cc_procedure"))
                       )
              )
            )
    ),
    tabItem(tabName = "stats_aepr",
            h2(textOutput("title_stats_aepr")),
            tabsetPanel(
              tabPanel("Trimestre 1", fluidRow(column(width = 12, br(), box(h3("Nombre d'avis"), status = "info", tableOutput('aepr_avis_trim_1'))))),
              tabPanel("Trimestre 2", fluidRow(column(width = 12, br(), box(h3("Nombre d'avis"), status = "info", tableOutput('aepr_avis_trim_2'))))),
              tabPanel("Trimestre 3", fluidRow(column(width = 12, br(), box(h3("Nombre d'avis"), status = "info", tableOutput('aepr_avis_trim_3'))))),
              tabPanel("Trimestre 4", fluidRow(column(width = 12, br(), box(h3("Nombre d'avis"), status = "info", tableOutput('aepr_avis_trim_4'))))),
              tabPanel("Annuel", fluidRow(column(width = 12, br(), box(h3("Nombre d'avis"), status = "info", tableOutput('aepr_avis_total_annuel'))))),
              tabPanel("Graphiques", 
                       fluidRow(
                         column(width = 12, h3("Répartition annuelle du nombre de dossiers")),
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepr_departement")),
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepr_domaine"))
                       ),
                       fluidRow(
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepr_categorie")),
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepr_procedure"))
                       )                       
                       
              )
            )
    ),
    tabItem(tabName = "stats_aepr_cc",
            h2(textOutput("title_stats_aepr_cc")),
            tabsetPanel(
              tabPanel("Trimestre 1", 
                       fluidRow(
                         column(width = 12, br(),
                                box(h3("Décisions signées"), status = "info", tableOutput('aepr_cc_decisions_signees_trim_1'),
                                    h3("Recours"), tableOutput('aepr_cc_recours_trim_1')
                                ),
                                box(
                                  h3("Sens des décisions sur le total des dossiers AEPR-CC"), status = "info",
                                  tableOutput('aepr_cc_sens_decisions_trim_1')
                                ))
                       )
              ),
              tabPanel("Trimestre 2", 
                       fluidRow(
                         column(width = 12, br(),
                                box(h3("Décisions signées"), status = "info", tableOutput('aepr_cc_decisions_signees_trim_2'),
                                    h3("Recours"), tableOutput('aepr_cc_recours_trim_2')),
                                box(
                                  h3("Sens des décisions sur le total des dossiers AEPR-CC"), status = "info",
                                  tableOutput('aepr_cc_sens_decisions_trim_2')
                                ))
                       )
              ),
              tabPanel("Trimestre 3", 
                       fluidRow(
                         column(width = 12, br(),
                                box(h3("Décisions signées"), status = "info", tableOutput('aepr_cc_decisions_signees_trim_3'),
                                    h3("Recours"), tableOutput('aepr_cc_recours_trim_3')),
                                box(
                                  h3("Sens des décisions sur le total des dossiers AEPR-CC"), status = "info",
                                  tableOutput('aepr_cc_sens_decisions_trim_3')
                                ))
                       )
              ),
              tabPanel("Trimestre 4", 
                       fluidRow(
                         column(width = 12, br(),
                                box(h3("Décisions signées"), status = "info", tableOutput('aepr_cc_decisions_signees_trim_4'),
                                    h3("Recours"), tableOutput('aepr_cc_recours_trim_4')),
                                box(
                                  h3("Sens des décisions sur le total des dossiers AEPR-CC"), status = "info",
                                  tableOutput('aepr_cc_sens_decisions_trim_4')
                                ))
                       )
              ),
              tabPanel("Annuel", 
                       fluidRow(
                         column(width = 12, br(),
                                box(h3("Décisions signées"), status = "info", tableOutput('aepr_cc_decisions_signees_total_annuel'),
                                    h3("Recours"), tableOutput('aepr_cc_recours_total_annuel')),
                                box(
                                  h3("Sens des décisions sur le total des dossiers AEPR-CC"), status = "info",
                                  tableOutput('aepr_cc_sens_decisions_total_annuel')
                                ))
                       )
              ),
              tabPanel("Graphiques", 
                       fluidRow(
                         column(width = 12, h3("Répartition annuelle du nombre de dossiers")),
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepr_cc_departement")),
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepr_cc_domaine"))
                       ),
                       fluidRow(
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepr_cc_categorie")),
                         box(width = 6, highchartOutput("hc_nb_dossiers_aepr_cc_procedure"))
                       )                       
                       
              )
            )
    ),
    tabItem(tabName = "previous_years",
            h2(textOutput("title_previous_years")),
            fluidRow(
              box(width = 6, highchartOutput("hc_stats_evolution_aepp")),
              box(width = 6, highchartOutput("hc_stats_evolution_aepp_cc"))
            ),
            fluidRow(
              box(width = 6, highchartOutput("hc_stats_evolution_aepr")),
              box(width = 6, highchartOutput("hc_stats_evolution_aepr_cc"))
            )             
    ),
    tabItem(tabName = "selected_year",
            h2(textOutput("title_selected_year")),
            fluidRow(
              box(width = 6, highchartOutput("hc_stats_evolution_aepp_annee")),
              box(width = 6, highchartOutput("hc_stats_evolution_aepp_cc_annee"))
            ),
            fluidRow(
              box(width = 6, highchartOutput("hc_stats_evolution_aepr_annee")),
              box(width = 6, highchartOutput("hc_stats_evolution_aepr_cc_annee"))
            )             
    ),    
    tabItem(tabName = "presentation", fluidRow(box(width=12,includeMarkdown("readme.md")))),
    tabItem(tabName = "glossary", fluidRow(box(width=12,includeMarkdown("glossary.md")))),
    tabItem(tabName = "changelog", fluidRow(box(width=12,includeMarkdown("changelog.md")))),
    tabItem(tabName = "roadmap", fluidRow(box(width=12,includeMarkdown("roadmap.md")))),
    tabItem(tabName = "legal_notice", fluidRow(box(width=12,includeMarkdown("legal_notice.md")))
    )
  )
)

# Put them together into a dashboardPage
dashboardPage(
  skin = "blue", 
  dashboardHeader(title = "Autorité environnementale en Pays de la Loire", titleWidth = 450),
  sidebar,
  body
)
