# Journal des modifications

## Version 1.2.2.4
Menu principal :
* Modification des intitulés des sous-items des menus des rubriques "Bilans d'activité" et "Statistiques"

Page d'accueil **"Chiffres-clés"** :
* Correction de l'affichage des chiffres-clés en cas de données incomplètes

Rubrique **"Bilans d'activité"** :
* Ajout d'une colonne `procedure` dans les tableaux

Rubrique **"Statistiques"** :
* Ajout de graphiques par procédure
* Création d'un tableau fusionnant les deux précédents tableaux par type d'avis et de dossiers pour les Avis Plans/Programmes
* Dédoublement du tableau "Répartition par département" pour distinguer les documents d'urbanisme des autres Plans/Programmes
* Correction du bug générant des exports des graphiques en noir & blanc
* Remplacement des valeurs NA par 0 dans les tableaux

## Version 1.2.2.3
* Actualisation du graphique d'évolution par trimestre pour l'année sélectionnée à la place de l'année en cours dans la rubrique **"Évolutions"**
* Ajout de l'année sélectionnée dans le titre des rubriques **"Bilans d'activité"**, **"Statistiques"** et **"Évolutions"**

Rubrique **"Statistiques"** :
* affichage de la mention "Aucune donnée disponible" pour les tableaux en cas d'absence de données pour un millésime
* masquage des éléments ayant la valeur zéro dans les graphiques

## Version 1.2.2.2
Modification du paramétrage de la connexion à la base de données :
* utilisation de la librairie *config*
* saisie des paramètres dans un fichier *config.yml*
* ajout d'un fichier d'exemple *config-sample.yml*
* utilisation des paramètres masqués dans le fichier *global.R*

## Version 1.2.2.1
Rubrique **"Évolutions"** :
* séparation en deux pages :
  - **"Années précédentes"** : Évolutions des avis et décisions de l'Autorité environnementale sur les années précédentes (ancienne page)
  - **"Année en cours"** : Évolutions des avis et décisions de l'Autorité environnementale par mois sur l'année en cours (ajout)
* indication des années concernées en sous-titre
Rubrique **"Statistiques"** :
* correction d'un bug qui permettait l'affichage d'étiquettes pour les valeurs égales à zéro

## Version 1.2.2

Rubrique **"Bilans d'activité"** :
* suppression de la pagination : tous les dossiers s'affichent par défaut
* paramétrage de la longueur de page (*pageLength*) pour 1000 enregistrements
* affichage de l'information sur le nombre d'enregsitrements en haut de page
* ajout d'un thème [Bootstrap](https://getbootstrap.com/) sur les tableaux
* affichage des dates au format *dd/mm/YYYY* dans les tableaux
* ajout de boutons permettant d'exporter les données :
  - **"Copier"** : copie les données dans le presse-papiers
  - **"Télécharger au format Excel"** : ouvre une fenêtre de téléchargement d'un fichier au format .xlsx contenant tous les enregistrements affichés sur la page
* ajout de filtres par colonne avec une liste déroulante pour les champs suivants (selon les types de consultation) :
  - **"Département"**
  - **"Chargé de mission"**
  - **"Domaine"**
  - **"Catégorie"**
  - **"Document d'urbanisme"**
  - **"Soumission EE"**
  - **"Trimestre"**

Les filtres des autres colonnes sont utilisables avec des [recherches plein texte](https://fr.wikipedia.org/wiki/Recherche_plein_texte).

## Version 1.2.1.1
* Ajout de la date d'export des données GARANCE sur la page d'accueil
* Ajout des rubriques :
  - **"Mentions légales"**
  - **"Nous contacter"** : lien *mailto* vers la boîte aux lettres fonctionnelle ADL

## Version 1.2.1

* Ajout de la sélection par millésime
* Ajout de la rubrique **"Développement"** composé des sous-rubriques :
   - **"Journal des modifications"** : liste des modifications par version de l'application
   - **"Feuille de route"** : liste des évoilutions envisagées par version de l'application
* Ajout de la rubrique **"Évolutions"** présentant des graphiques d'évolution du nombre de dossiers par type de consultation et par millésime :
  - graphiques de répartition par type d'avis (exprès ou tacite) pour les AEPP et AEPR
  - graphiques de répartition par type de décision (soumission/dispense) pour les cas par cas
* Ajout de la fonctionnalité d'export des graphiques aux formats image, PDF et CSV
